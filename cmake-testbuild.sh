#!/bin/sh

# This script tests the CMake build by:
# - builds the main CMakeLists.txt
# - builds the unit test again in a separate build tree so
#   the config-module will also be tested

set -ex

cmake -H. -Bout/build/nunicode -DCMAKE_INSTALL_PREFIX=${PWD}/out -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=$BUILD_SHARED_LIBS
cmake --build out/build/nunicode --target install --config Release

cmake -Htests -Bout/build/tests -DCMAKE_INSTALL_PREFIX=${PWD}/out -DCMAKE_PREFIX_PATH=${PWD}/out -DCMAKE_BUILD_TYPE=Release
cmake --build out/build/tests --target install --config Release

out/bin/units

