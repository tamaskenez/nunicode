set(LIBNU_SRC
	cesu8.c
	ducet.c
	extra.c
	strcoll.c
	strings.c
	tofold.c
	tolower.c
	toupper.c
	utf16.c
	utf16be.c
	utf16he.c
	utf16le.c
	utf32.c
	utf32be.c
	utf32he.c
	utf32le.c
	utf8.c
	validate.c
	version.c
)

add_library(nunicode STATIC ${LIBNU_SRC})
target_include_directories(nunicode PUBLIC
	$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}>
	$<INSTALL_INTERFACE:include>)
target_compile_definitions(nunicode
	PUBLIC
		${NU_BUILD_OPTIONS} NU_BUILD_STATIC)
set_target_properties(nunicode PROPERTIES
	OUTPUT_NAME nu
	DEBUG_POSTFIX d)

file(GLOB INCLUDES *.h)
install(FILES ${INCLUDES} DESTINATION include/libnu)

install(TARGETS nunicode DESTINATION lib
	EXPORT nunicode-targets)
install(EXPORT nunicode-targets DESTINATION lib/cmake/nunicode
	FILE nunicode-config.cmake)

